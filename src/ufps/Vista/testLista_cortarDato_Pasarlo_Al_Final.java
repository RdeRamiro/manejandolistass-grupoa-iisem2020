/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import java.util.Scanner;
import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author LUISD
 */
public class testLista_cortarDato_Pasarlo_Al_Final {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        ListaS<String> nombres=new ListaS();
        nombres.insertarInicio("marco");
        nombres.insertarInicio("gederson");
        nombres.insertarInicio("diana");
        nombres.insertarInicio("carlos");
        nombres.insertarInicio("Ramiro");
        
        System.out.println("Orden Actual de la lista es: " +nombres.toString());
        System.out.println("Digite el nombre a mover");
        while(in.hasNext()){
        String s = in.nextLine();
        nombres.cortarDato_pasarlo_Al_Final(s);
        System.out.println("Orden Actualizado de la lista es: " + nombres.toString());}
        
        
       }
    
}
