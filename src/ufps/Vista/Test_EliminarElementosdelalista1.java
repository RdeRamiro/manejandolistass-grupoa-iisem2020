/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import java.util.Scanner;
import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author LUISD
 */
public class Test_EliminarElementosdelalista1 {
    
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        ListaS<Character> lista=new ListaS();
        lista.insertarFinal('a');
        lista.insertarFinal('b');
        lista.insertarFinal('c');
        lista.insertarFinal('d');
        lista.insertarFinal('e');
        lista.insertarFinal('f');
        lista.insertarFinal('f');
        lista.insertarFinal('f');
        lista.insertarFinal('i');
        lista.insertarFinal('j');
        lista.insertarFinal('k');
        lista.insertarFinal('a');
        
        System.out.println(lista.toString() + " " +lista.getCardinalidad());
        lista.eliminarRepetidos();
        System.out.println(lista.toString() + " " +lista.getCardinalidad());
        
        
    }
}
