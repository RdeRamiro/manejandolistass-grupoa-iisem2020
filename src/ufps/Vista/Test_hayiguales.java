/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import java.util.Scanner;
import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author LUISD
 */
public class Test_hayiguales {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        ListaS<Character> lista1=new ListaS();
        char x='a';
        for (int i = 0; i <5; i++) {
            lista1.insertarFinal(x++);
        }
        ListaS<Character> lista2=new ListaS();
        char e='f';
        for (int i = 0; i < 5; i++) {
            lista2.insertarFinal(e++);
        }
        System.out.println("Caso 1. No hay elementos repetidos: " +lista1.hayRepetido(lista1, lista2));
        ListaS<Character> lista3=new ListaS();
        char xy='a';
        for (int i = 0; i <5; i++) {
            lista3.insertarFinal(xy++);
        }
        ListaS<Character> lista4=new ListaS();
        char ey='e';
        for (int i = 0; i < 5; i++) {
            lista4.insertarFinal(ey++);
        }
        System.out.println("Caso 2. Hay elementos repetidos: " +lista3.hayRepetido(lista3, lista4));
    }
}
