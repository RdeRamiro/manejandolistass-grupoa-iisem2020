/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author LUISD
 */
public class Test_Union {
    
    public static void main(String[] args) {
        
        /**
         * Prueba de Union de conjuntos matematica
         */
        ListaS<Integer> lista1 = new ListaS();
        lista1.insertarFinal(4);
        lista1.insertarFinal(5);
        lista1.insertarFinal(12);
        lista1.insertarFinal(13);
        lista1.insertarFinal(42);
        System.out.println(lista1.toString());
        
        ListaS<Integer> lista2 = new ListaS();
        lista2.insertarFinal(4);
        lista2.insertarFinal(6);
        lista2.insertarFinal(7);
        lista2.insertarFinal(8);
        lista2.insertarFinal(7);
        lista2.insertarFinal(5);
        System.out.println(lista2.toString());
        
        lista1.unirConIdeaAlferez(lista2);
        System.out.println("La nueva lista de alferez: " +lista1.toString());
        
        /**
         * Prueba de Union de conjuntos matematica ascendentemente
         */
        ListaS<Integer> l1 = new ListaS();
        l1.insertarFinal(4);
        l1.insertarFinal(5);
        l1.insertarFinal(12);
        l1.insertarFinal(13);
        l1.insertarFinal(42);
        System.out.println(l1.toString());
        
        ListaS<Integer> l2 = new ListaS();
        l2.insertarFinal(4);
        l2.insertarFinal(6);
        l2.insertarFinal(7);
        l2.insertarFinal(8);
        l2.insertarFinal(7);
        l2.insertarFinal(5);
        System.out.println(l2.toString());
        
        ListaS<Integer> l4 = l1.unircon(l2);
        System.out.println("La lista unida: " +l4.toString());
        l4.unirAscendentemente();
        System.out.println("La lista ordenada ramiro: " +l4.toString());
        
    }
}
