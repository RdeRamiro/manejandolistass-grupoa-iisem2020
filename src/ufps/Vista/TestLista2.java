/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class TestLista2 {
    public static void main(String[] args) {
        ListaS<Integer> enteros=new ListaS();
        enteros.insertarFinal(20);
        enteros.insertarFinal(200);
        enteros.insertarFinal(2000);
        enteros.insertarFinal(20000);
        System.out.println(enteros.toString());
        System.out.println("El dato de la posición 2 es:"+enteros.get(2));
        System.out.println("El dato de la posición 30 es:"+enteros.get(30));
        
        System.out.println("Actualizando datos");
        enteros.set(2, 7000);
        enteros.set(20, 17000);
        System.out.println(enteros.toString());
        
        System.out.println("Borrando lista:");
        enteros.borrarTodo();
        System.out.println(enteros.toString());
         
    }
}
